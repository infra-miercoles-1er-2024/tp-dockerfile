# TP Dockerfile

## Mi App

Dockerfile para la creacion y despliegue de la aplicacion **Mi App**.



### Creación de la imagen

#### Clonar repositorio

```
$ git clone https://gitlab.com/infra-miercoles-1er-2024/tp-dockerfile.git
```

 Una vez clonado el repo acceder al directorio.

```
$ cd tp-dockerfile
```

#### Creación de imagen

```
$ docker build -t mi-app-img .
```

> Nota: Antes de construir la imagen posicionarse en la rama correspondiente (main, develop, testing).

#### Despliegue del contenedor

Mediante comandos Docker.

```
$ docker run -d -p 8000:80 --name mi-app mi-app-img
```

Mediante compose.

```
$ docker-compose up -d
```

### Proxy reverso

Añadir esta configuración al servidor proxy reverso.

```
server {
    listen 80;
    server_name mi-app.com;

    location / {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_pass http://ip_docker_server:8000;
    }
}
```

> Nota: Archivo de vitualhost de nginx 'mi-app.conf' .
